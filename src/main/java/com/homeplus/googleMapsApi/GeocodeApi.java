package com.homeplus.googleMapsApi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Array;

@Component
public class GeocodeApi {

 public Integer getDistanceBetweenTasks(String[] address, String[] address2) throws IOException, InterruptedException, ApiException {
     String api_key = "AIzaSyDqbnjFcY4yyRcIpDc7hqB4sL1vDtdwU8I";
     GeoApiContext context = new GeoApiContext.Builder()
             .apiKey(api_key)
             .build();

     DistanceMatrix distanceResults = DistanceMatrixApi.getDistanceMatrix(context, address, address2).await();

     Integer distance = Math.toIntExact(distanceResults.rows[0].elements[0].distance.inMeters);

     context.shutdown();
        try {
            return distance;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
 }

 public double[] getLngLat(String address) throws IOException, InterruptedException, ApiException {
     String api_key = "AIzaSyCQ4w184gu8rK0fI73d6xSvxQY-1uhUixg";
     GeoApiContext context = new GeoApiContext.Builder()
             .apiKey(api_key)
             .build();

     double latLng[];
     latLng = new double[2];
     GeocodingResult[] results =  GeocodingApi.geocode(context,
             address).await();

     LatLng location = results[0].geometry.location;

     context.shutdown();
     double lat = location.lat;
     double lng = location.lng;
     System.out.println(location);
     latLng[0] = lat;
     latLng[1] = lng;
     try {
         return latLng;
     } catch (Exception e) {
         e.printStackTrace();
         return null;
     }
 }

}
