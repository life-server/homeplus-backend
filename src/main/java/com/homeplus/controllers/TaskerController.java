package com.homeplus.controllers;

import com.homeplus.dtos.tasker.TaskerGetDto;
import com.homeplus.dtos.tasker.TaskerPostDto;
import com.homeplus.dtos.tasker.TaskerPutDto;
import com.homeplus.services.TaskerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@Validated
public class TaskerController {

    private final TaskerService taskerService;

    @GetMapping("taskers")
    public ResponseEntity<List<TaskerGetDto>> find(){
        return ResponseEntity.ok(taskerService.getAllTaskers());
    }

    @GetMapping("tasker")
    public ResponseEntity<TaskerGetDto> find(@RequestParam Long id){
        return ResponseEntity.ok(taskerService.getTaskerById(id));
    }

    @GetMapping("user-tasker")
    public ResponseEntity<TaskerGetDto> findByUserId(@RequestParam Long id){
        return ResponseEntity.ok(taskerService.getTaskerByUserId(id));
    }

    @PostMapping("create-tasker")
    public ResponseEntity<String> createTasker(@Valid @RequestBody TaskerPostDto taskerPostDto) {
        taskerService.createTasker(taskerPostDto);
        return ResponseEntity.ok("success");
    }

    @PutMapping("update-tasker")
    public ResponseEntity<String> updateTasker(@Valid  @RequestBody TaskerPutDto taskerPutDto){
        taskerService.updateTasker(taskerPutDto);
        return ResponseEntity.ok("tasker has been updated");
    }
}
