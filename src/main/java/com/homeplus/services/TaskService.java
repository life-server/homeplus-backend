package com.homeplus.services;

import com.google.maps.errors.ApiException;
import com.homeplus.dtos.task.TaskGetDto;
import com.homeplus.dtos.task.TaskPostDto;
import com.homeplus.dtos.task.TaskPutDto;
import com.homeplus.googleMapsApi.GeocodeApi;
import com.homeplus.models.*;
import com.homeplus.repositories.TaskOfferRepository;
import com.homeplus.repositories.TaskRepository;
import com.homeplus.utility.mapper.TaskMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;
    private final UserService userService;
    private final TaskerService taskerService;
    private final TaskOfferRepository taskOfferRepository;
    private final TimeSectionsService timeSectionsService;

    private final GeocodeApi geocodeApi;

    public List<TaskGetDto> getAllTasks() {
        return taskRepository.findAll().stream()
                .map(task -> taskMapper.fromEntity(task))
                .filter(task -> task.getDate().compareTo(OffsetDateTime.now()) > 0)
                .collect(Collectors.toList());
    }

    public TaskGetDto getTaskById(Long id) {
        return taskMapper.fromEntity(this.getTaskEntityById(id));
    }

    public TaskEntity getTaskEntityById(Long id){
        return taskRepository.findById(id).get();
    }

    public List<TaskGetDto> getUpComingTasksByUid(Long uid) {
        UserEntity userEntity = userService.findUserById(uid);
        OffsetDateTime currentDate = OffsetDateTime.now();
        OffsetDateTime upComingDay = OffsetDateTime.now().plusDays(3);
        return taskRepository.findUpComingTasksByUid(userEntity, currentDate, upComingDay)
                .stream()
                .map(taskMapper::taskToTaskGetDto)
                .collect(Collectors.toList()
                );
    }

    public List<TaskGetDto> getUpComingTasksByTid(Long tasker_id) {
        TaskerEntity taskerEntity = taskerService.getTaskerEntityById(tasker_id);
        OffsetDateTime currentDate = OffsetDateTime.now();
        OffsetDateTime upComingDay = OffsetDateTime.now().plusDays(3);
        return taskRepository.findUpComingTasksByTid(taskerEntity, currentDate, upComingDay)
                .stream()
                .map(taskMapper::taskToTaskGetDto)
                .collect(Collectors.toList()
                );
    }

    public List<TaskGetDto> getTasksByUidAndKeyword(Long uid, String keyword) {
        UserEntity userEntity = userService.findUserById(uid);
        if(!keyword.isEmpty()){
            return searchTasksByKeyword(keyword).stream()
                    .filter(task -> task.getUserEntity() == userEntity)
                    .collect(Collectors.toList());
        } else {
            return taskRepository.findByUserId(userEntity)
                .stream()
                .map(taskMapper::taskToTaskGetDto)
                .collect(Collectors.toList()
                );
        }

    }

    public List<TaskGetDto> getTasksByTidAndKeyword(Long tasker_id, String keyword) {
        TaskerEntity taskerEntity = taskerService.getTaskerEntityById(tasker_id);

        if(!keyword.isEmpty()){
            return searchTasksByKeyword(keyword).stream()
                    .filter(task -> task.getTaskerEntity() == taskerEntity)
                    .collect(Collectors.toList());
        } else {
            return taskRepository.findByTaskerId(taskerEntity)
                .stream()
                .map(taskMapper::taskToTaskGetDto)
                .collect(Collectors.toList()
                );
        }

    }

    public void createTask(TaskPostDto taskPostDto) throws IOException, InterruptedException, ApiException {
        taskPostDto.setUserEntity(userService.findUserById(taskPostDto.getUser_id()));
        taskPostDto.setTitle(taskPostDto.getTitle().toLowerCase());
        taskPostDto.setPaid(false);

        String address = String.join(" ",
                taskPostDto.getStreet(),
                taskPostDto.getSuburb(),
                taskPostDto.getPostcode().toString() ,
                taskPostDto.getState());
        double latLng[] = geocodeApi.getLngLat(address);
        taskPostDto.setLat(latLng[0]);
        taskPostDto.setLng(latLng[1]);

        if (taskPostDto.getCertain_time() != 0) {
            taskPostDto.setTimeSectionsEntity(timeSectionsService.getTimeById(taskPostDto.getCertain_time()));
        }
        TaskEntity taskEntity = taskMapper.postDtoToEntity(taskPostDto);
        taskRepository.save(taskEntity);
    }

    public void deleteTask(Long id) {
        boolean taskExist = taskRepository.findById(id).isPresent();
        if(taskExist){
            taskRepository.deleteById(id);
        } else {
            throw new IllegalStateException("task is not exist");
        }
    }


    public void updateTask(TaskPutDto taskPutDto) {
        boolean taskExist = taskRepository.findById(taskPutDto.getId()).isPresent();
        if(taskExist){
            if (taskPutDto.getCertain_time() != null) {
                if (taskPutDto.getCertain_time() != 0){
                    taskPutDto.setTimeSectionsEntity(timeSectionsService.getTimeById(taskPutDto.getCertain_time()));
                }
            }

            taskRepository.save(taskMapper.putDtoToEntity(taskPutDto));
        } else {
            throw new IllegalStateException("task is not exist");
        }
    }

    public void updateTaskerOfTask(TaskOfferEntity offer, String price, TaskStatus task_status) {
        TaskEntity task = offer.getTaskEntity();
        task.setPrice(price);
        if (task_status == TaskStatus.assigned) {
            task.setTaskerEntity(offer.getTaskerEntity());
        }

        if (task_status == TaskStatus.open) {
            task.setTaskerEntity(null);
        }

        task.setTask_status(task_status);
        taskRepository.save(task);
    }

    public List<TaskGetDto> getNearbyTasks(String address, Integer meters, List<TaskGetDto> tasks) {
        return tasks.stream()
                .filter(task ->
                {
                    try {
                        return getDistanceBetweenTasks(address,
                                String.join(" ",
                                        task.getSuburb(),
                                        task.getPostcode().toString() ,
                                        task.getState())) < meters;
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ApiException e) {
                        e.printStackTrace();
                    }
                    return false;
                })
                .collect(Collectors.toList());
    }

    public Integer getDistanceBetweenTasks(String address, String address2) throws IOException, InterruptedException, ApiException {
        return geocodeApi.getDistanceBetweenTasks(new String[]{address}, new String[]{address2});
    }

    public List<TaskGetDto> searchTasksByConditions(String address, Integer meters, String keyword) {
        List<TaskGetDto> searchedTasks = getAllTasks();
        if(!keyword.isEmpty()){
            searchedTasks = searchTasksByKeyword(keyword);
        }

//        if (!address.isEmpty() && meters >= 1000) {
//            searchedTasks = getNearbyTasks(address, meters, searchedTasks);
//        }

        return searchedTasks;
    }

    public List<TaskGetDto> TasksToGetDto(List<TaskEntity> tasks){
        return tasks.stream()
                .map(task -> taskMapper.fromEntity(task))
                .collect(Collectors.toList());
    }

    public List<TaskGetDto> searchTasksByKeyword(String keyword) {
        List<TaskGetDto> searchedTasks = new ArrayList<>();
        String[] keywords = keyword.trim().toLowerCase().split(" ");
        for (String key: keywords) {
            List<TaskGetDto> keyTasks = TasksToGetDto(taskRepository.findByKeyword(key));
            for (TaskGetDto task: keyTasks) {
                if (!searchedTasks.contains(task)){
                    searchedTasks.add(task);
                } else {
                    continue;
                }
            }
        }
        return searchedTasks;
    }

    public List<TaskEntity> getTasksByTasker(TaskerEntity taskerEntity) {
        return taskRepository.getTasksByTasker(taskerEntity);
    }

    public void cancelTask(Long id) {
        TaskEntity task = getTaskEntityById(id);
        taskOfferRepository.cancelOfferByTask(task, OffsetDateTime.now());
        task.setTask_status(TaskStatus.open);
        task.setTaskerEntity(null);

        taskRepository.save(task);
    }
}
