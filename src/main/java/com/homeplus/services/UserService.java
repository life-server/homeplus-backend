package com.homeplus.services;

import com.homeplus.config.FrontEndUrlConfig;
import com.homeplus.dtos.AccountDto;
import com.homeplus.dtos.user.UserGetDto;
import com.homeplus.dtos.user.UserPutDto;
import com.homeplus.email.EmailSender;
import com.homeplus.exceptions.UserNotFoundException;
import com.homeplus.models.ConfirmationTokenEntity;
import com.homeplus.models.UserEntity;
import com.homeplus.repositories.ConfirmationTokenRepository;
import com.homeplus.repositories.UserRepository;
import com.homeplus.utility.mapper.ConfirmationTokenMapper;
import com.homeplus.utility.mapper.UserGetMapper;
import com.homeplus.utility.mapper.UserInfoMapper;
import com.homeplus.utility.mapper.UserMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.Date;

import java.util.UUID;

import static java.time.ZoneOffset.UTC;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final UserInfoMapper userInfoMapper;
    private final FrontEndUrlConfig frontEndUrlConfig;
    private final UserGetMapper userGetMapper;
    private final ConfirmationTokenMapper tokenMapper;
    private final ConfirmationTokenRepository confirmationTokenRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final EmailSender emailSender;

    private static final long jwtExpireTime = 1000 * 60 * 60 * 24;


    public UserEntity getUserById(Long id) {
        return userRepository.findById(id).get();
    }

    public UserGetDto getUserDtoById(Long id) {
        return userInfoMapper.fromEntity(getUserById(id));
    }

    @Value("${jwt.secret}")
    private String jwtSecret;

    @Value("${jwt.secretKey}")
    private String jwtSecretKey;

    public boolean ifEmailExists(String email) {
        return userRepository.findByEmail(email).isPresent();
    }

    @Transactional
    public void register(AccountDto accountDto) {
        UserEntity userEntity = userMapper.mapInfoDtoToEntity(accountDto);
        String encodedPassword = bCryptPasswordEncoder.encode(accountDto.getPassword());
        String token = this.generateRegisterToken();
        userEntity.setPassword(encodedPassword);
        log.info("userEntity: " + userEntity);
        userRepository.save(userEntity);
        log.info("Save user successfully!");

        ConfirmationTokenEntity confirmationTokenEntity = tokenMapper.mapTokenDtoToEntity(accountDto);
        confirmationTokenEntity.setToken(token);
        confirmationTokenRepository.save(confirmationTokenEntity);

        String email = accountDto.getEmail();
        String link = this.generateEmailVerifyLink(token);
        emailSender.send(email, this.buildEmail(accountDto.getName(), link));
        log.info("Email send successfully!");
    }

    public String generateRegisterToken(){
        return UUID.randomUUID().toString();
    }

    public String generateEmailVerifyLink(String token) {
        String frontEndUrl = frontEndUrlConfig.getFrontEndUrl();
//        String verifyLink = frontEndUrl + "/verifylink/verify?code=" + token;
        String verifyLink = frontEndUrl + "/account-verify/" + token;

        log.info("verifyLink: {}", verifyLink);
        return verifyLink;
    }

    @Transactional
    public String updateUserStatusAndUpdateTokenToNull(String token) {
        boolean tokenExist = confirmationTokenRepository.findByToken(token).isPresent();
        if(!tokenExist) {
            throw new IllegalStateException("token not exist");
        }
        ConfirmationTokenEntity confirmationTokenEntity = confirmationTokenRepository.findByToken(token).get();
        OffsetDateTime expireTime = confirmationTokenEntity.getExpireTime();
        if(expireTime.isBefore(OffsetDateTime.now())){
            throw new IllegalStateException("token expired");
        }
        try {
            confirmationTokenRepository.updateTokenToNull(confirmationTokenEntity.getEmail());
            userRepository.updateStatusByEmail(confirmationTokenEntity.getEmail());
        }catch (Exception e) {
            log.error("Failed to confirm ConfirmationTokenEntity");
            throw new IllegalStateException("failed to confirm email");
        }
        return "confirmed";
    }

    @Transactional
    public void resendConfirmationLink(String token) {
        boolean userNotConfirmed = confirmationTokenRepository.findByToken(token).isPresent();
        if(!userNotConfirmed) {
            throw new IllegalStateException("user token not exist. please check user verify status");
        }
        ConfirmationTokenEntity confirmationTokenEntity = confirmationTokenRepository.findByToken(token).get();
        confirmationTokenEntity.setCreateTime(OffsetDateTime.now());
        confirmationTokenEntity.setExpireTime(OffsetDateTime.now().plusHours(1));
        confirmationTokenRepository.save(confirmationTokenEntity);

        String email = confirmationTokenEntity.getEmail();
        UserEntity userEntity = userRepository.findByEmail(email).get();

        String link = this.generateEmailVerifyLink(token);
        emailSender.send(email, this.buildEmail(userEntity.getName(), link));
        log.info("Email send successfully!");

    }

    @Transactional
    public String generateResetLink(String email) {
        String frontEndUrl = frontEndUrlConfig.getFrontEndUrl();

        boolean tokenExist = confirmationTokenRepository.findByEmail(email).isPresent();
        if(!tokenExist) {
            throw new IllegalStateException("token entity not exist" + email);
        }
        ConfirmationTokenEntity confirmationTokenEntity = confirmationTokenRepository.findByEmail(email).get();
        String token = UUID.randomUUID().toString();
        confirmationTokenEntity.setToken(token);
        confirmationTokenEntity.setCreateTime(OffsetDateTime.now());
        confirmationTokenEntity.setExpireTime(OffsetDateTime.now().plusHours(1));
        confirmationTokenRepository.save(confirmationTokenEntity);
        String resetPasswordLink = frontEndUrl + "/password-reset/" + token;

        log.info("Reset password link: {}", resetPasswordLink);
        return resetPasswordLink;
    }

    public void sendResetPasswordEmail(String email) {
        boolean userExist = userRepository.findByEmail(email).isPresent();
        if(!userExist) {
            throw new IllegalStateException("user not exist: " + email);
        }
        UserEntity userEntity = userRepository.findByEmail(email).get();

        String link = this.generateResetLink(email);
        emailSender.send(email, this.buildResetPasswordEmail(userEntity.getName(), link));
        log.info("Reset password email send successfully!");

    }

    public String getEmailByToken(String token) {
        boolean tokenExist = confirmationTokenRepository.findByToken(token).isPresent();
        if(!tokenExist) {
            throw new IllegalStateException("token not exist");
        }
        String email = confirmationTokenRepository.findByToken(token).get().getEmail();
        return email;
    }

    @Transactional
    public void resetPassword(AccountDto accountDto) {
        boolean userExist = userRepository.findUserEntityByEmail(accountDto.getEmail()).isPresent();
        if(!userExist) {
            throw new IllegalStateException("User with email not exist:" + accountDto.getEmail());
        }
        boolean tokenEntityExist = confirmationTokenRepository.findByEmail(accountDto.getEmail()).isPresent();
        if(!tokenEntityExist) {
            throw new IllegalStateException("token entity with this email not exist: " + accountDto.getEmail());
        }
        OffsetDateTime tokenExpireTime = confirmationTokenRepository
                .findByEmail(accountDto.getEmail()).get().getExpireTime();
        if(OffsetDateTime.now().isAfter(tokenExpireTime)) {
            throw new IllegalStateException("token expired");
        }

        confirmationTokenRepository.updateTokenToNull(accountDto.getEmail());

        UserEntity userEntity = userRepository.findByEmail(accountDto.getEmail()).get();
        String encodedPassword = bCryptPasswordEncoder.encode(accountDto.getPassword());
        userEntity.setPassword(encodedPassword);
        userRepository.save(userEntity);

        log.info("Save user successfully!");

    }


    public boolean isPasswordCorrect(String email, String password) {
        boolean userExist = userRepository.findByEmail(email).isPresent();
        if(!userExist) {
            throw new IllegalStateException("user not exist: " + email);
        }
        UserEntity userEntity = userRepository.findUserEntityByEmail(email).get();
        String encodedPassword = userEntity.getPassword();

        return bCryptPasswordEncoder.matches(password, encodedPassword);
    }

    public String generateJws(String email, String secretKey) {
        String jwtToken = Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setHeaderParam("alg", "HS512")
                .setSubject("login")
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + jwtExpireTime))
                .claim("email", email)
                .signWith(Keys.hmacShaKeyFor(secretKey.getBytes()))
                .compact();
        log.info("jwt token: " + jwtToken);
        return jwtToken;
    }

    public Claims parseJwt(String jwt, String secretKey) {
        Claims body;
        try {
            Jws<Claims> claimsJws = Jwts.parserBuilder()
                    .setSigningKey(Keys.hmacShaKeyFor(secretKey.getBytes()))
                    .build()
                    .parseClaimsJws(jwt);
            body = claimsJws.getBody();
        }catch (Exception e) {
            throw new IllegalStateException("parse JWT failed: " + jwt);
        }

        return body;
    }

    public boolean validateJwt(String jwt, String secretKey){
        String email;
        try {
            Claims body = this.parseJwt(jwt, secretKey);
            email = String.valueOf(body.get("email"));
        } catch (Exception e) {
            return false;
        }

        return !email.equals("");
    }

    public boolean isJwtValid(String jwt, String secretKey) {
        try {
            Claims body = this.parseJwt(jwt, secretKey);
            Date exp = body.getExpiration();
//            Long exp = body.getExpiration().getTime();
            Date current = new Date(System.currentTimeMillis());

            log.info(exp.toString());
            log.info(String.valueOf(System.currentTimeMillis()));
//            if (exp <= System.currentTimeMillis()) {
            if(exp.before(current)) {
                return false;
            }
        }catch (Exception e){
            return false;
        }

        return true;
    }

    public String getEmailFromJwtToken(String jwt, String secretKey) {
        Claims body = parseJwt(jwt, secretKey);
        String email = String.valueOf(body.get("email"));

        return email;
    }

    public String findVerifyStatus(String email) {
        return userRepository.findUserVerifiedStatusByEmail(email);
    }

    public boolean isUserExists(String email) {
        return userRepository.findByEmail(email).isPresent();
    }


    private UserEntity findUserByEmail(String email) {
        boolean userExists = userRepository.findByEmail(email).isPresent();
        if(!userExists) {
            throw new IllegalStateException("User with this email not exists: " + email);
        }else{
            return userRepository.findByEmail(email).get();
        }
    }

    public UserGetDto findUserGetDtoByEmail(String email) {
        return userGetMapper.getUserGetDtoFromEntity(userRepository.findUserEntityByEmail(email).get());
    }



    public UserEntity findUserById(Long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("Cannot find user with id: " + userId));
    }

    public void turnOnOffTasker(Long id) {
        UserEntity user = findUserById(id);
        if(!user.getIs_tasker()){
            userRepository.turnOnTasker(id);
        } else {
            userRepository.turnOffTasker(id);
        }

    }

    public void updateUser(UserPutDto userPutDto) {
        UserEntity user = findUserById(userPutDto.getId());
        userPutDto.setPassword(user.getPassword());
        userPutDto.setUpdated_time(OffsetDateTime.now(UTC));

        userRepository.save(userInfoMapper.putDtoToEntity(userPutDto));
    }

    public void updateIsTaskerData(Long id) {
        userRepository.updateIsTaskerData(id);
    }

    private String buildEmail(String name, String link) {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your email</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> Thank you for registering. Please click on the below link to activate your account: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\"" + link + "\">Activate Now</a> </p></blockquote>\n Link will expire in 60 minutes. <p>See you soon</p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "       <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "           <p style=\"Margin:0 0 5px 0;font-size:16px;line-height:25px;color:#0b0c0c\">Thank you for choosing HomePlus.</p><p style=\"Margin:0 0 20px 0;font-size:16px;line-height:25px;color:#0b0c0c\"> -The HomePlus Team </p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }

    private String buildResetPasswordEmail(String name, String link) {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Change your password</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> Thank you for joining us. Please click on the below link to reset your password: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\"" + link + "\">Change Now</a> </p></blockquote>\n Link will expire in 60 minutes. <p>See you soon</p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "       <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "           <p style=\"Margin:0 0 5px 0;font-size:16px;line-height:25px;color:#0b0c0c\">Thank you for choosing HomePlus.</p><p style=\"Margin:0 0 20px 0;font-size:16px;line-height:25px;color:#0b0c0c\"> -The HomePlus Team </p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }


}
