package com.homeplus.dtos.task;

import com.homeplus.models.TaskStatus;
import com.homeplus.models.TaskerEntity;
import com.homeplus.models.TimeSectionsEntity;
import com.homeplus.models.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskPutDto {

    private Long id;

    private UserEntity userEntity;

    private TaskerEntity taskerEntity;

    private String title;

    private String category;

    private OffsetDateTime date;

    private Long certain_time;

    private TimeSectionsEntity timeSectionsEntity;

    private Long budget;

    private String price;

    private String state;

    private String street;

    private String suburb;

    private Long postcode;

    private String house_type;

    private Integer num_of_rooms;

    private Integer num_of_bathrooms;

    private Integer levels;

    private Boolean lift;

    private Boolean in_person;

    private String item_name;

    private String item_value;

    private String description;

    private String review;

    private Integer rating;

    private TaskStatus task_status;

    private Boolean paid;

    private Double lng;

    private Double lat;

    private OffsetDateTime created_time;

    private final OffsetDateTime updated_time = OffsetDateTime.now();
}
