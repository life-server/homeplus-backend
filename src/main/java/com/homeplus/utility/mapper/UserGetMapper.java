package com.homeplus.utility.mapper;

import com.homeplus.dtos.user.UserGetDto;
import com.homeplus.models.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserGetMapper {

    UserGetDto getUserGetDtoFromEntity(UserEntity userEntity);

}
