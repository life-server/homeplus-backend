package com.homeplus.repositories;

import com.homeplus.models.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

@Repository
@EnableJpaRepositories
public interface TaskOfferRepository extends JpaRepository<TaskOfferEntity, Long> {

    @Query("select t from TaskOfferEntity as t where t.id = :id")
    Optional<TaskOfferEntity> findById(@Param("id") Long id);

    @Query("select o from TaskOfferEntity as o where o.taskEntity = :taskEntity")
    List<TaskOfferEntity> findOffersByTask(TaskEntity taskEntity);

    @Query("select o from TaskOfferEntity as o " +
            "where o.taskEntity = :taskEntity and " +
            "o.taskerEntity = :taskerEntity and " +
            "o.offer_status != 'CANCELED' and " +
            "o.offer_status != 'REJECTED'")
    Optional<TaskOfferEntity> findOfferByTaskAndTasker(TaskEntity taskEntity, TaskerEntity taskerEntity);

    @Transactional
    @Modifying
    @Query("update TaskOfferEntity as o " +
            "set o.offer_status = :status, " +
            "o.reply_message = :reply_msg, " +
            "o.updated_time = :now " +
            "where o.id = :id")
    void acceptOrRejectOffer(Long id, String reply_msg, TaskOfferStatus status, OffsetDateTime now);

    @Query("select o from TaskOfferEntity as o where o.taskEntity = :taskEntity and o.offer_status = 'PENDING'")
    Optional<TaskOfferEntity> getPendingOffers(TaskEntity taskEntity);

    @Query("select o from TaskOfferEntity as o where o.taskerEntity = :taskerEntity and o.offer_status = 'ACCEPTED'")
    Optional<TaskOfferEntity> getAcceptedOffers(TaskerEntity taskerEntity);

    @Transactional
    @Modifying
    @Query("update TaskOfferEntity as o " +
            "set o.offer_status = 'CANCELED', " +
            "o.reply_message = 'canceled by tasker', " +
            "o.updated_time = :now " +
            "where o.taskEntity = :task and " +
            "o.offer_status = 'ACCEPTED'")
    void cancelOfferByTask(TaskEntity task, OffsetDateTime now);
}
