ALTER TABLE "task_post" ADD "price" VARCHAR(255);

ALTER TABLE "task_post" ADD "paid" BOOL DEFAULT FALSE;

ALTER TABLE "task_post"
DROP COLUMN "review",
DROP COLUMN "rating";